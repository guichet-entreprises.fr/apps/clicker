/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.clicker.ws.v1.bean;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The algo.
 *
 * @author amboussa
 */
@XmlRootElement(name = "event", namespace = "http://v1.ws.clicker.common.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseEventBean {

    /** The id. */
    private Long id;

    /** The created. */
    private Calendar created;

    /** The updated. */
    private Calendar updated;

    /**
     * the ref id of event
     */

    private String reference;

    private String comment;

    private long counter;

    private Calendar effectDate;

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Accesseur sur l'attribut {@link #created}.
     *
     * @return Calendar created
     */
    public Calendar getCreated() {
        return created;
    }

    /**
     * Mutateur sur l'attribut {@link #created}.
     *
     * @param created
     *            la nouvelle valeur de l'attribut created
     */
    public void setCreated(Calendar created) {
        this.created = created;
    }

    /**
     * Accesseur sur l'attribut {@link #updated}.
     *
     * @return Calendar updated
     */
    public Calendar getUpdated() {
        return updated;
    }

    /**
     * Mutateur sur l'attribut {@link #updated}.
     *
     * @param updated
     *            la nouvelle valeur de l'attribut updated
     */
    public void setUpdated(Calendar updated) {
        this.updated = updated;
    }

    public String getReference() {
        return this.reference;
    }

    public ResponseEventBean setReference(final String reference) {
        this.reference = reference;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public ResponseEventBean setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public Calendar getEffectDate() {
        return effectDate;
    }

    public ResponseEventBean setEffectDate(Calendar effectDate) {
        this.effectDate = effectDate;
        return this;
    }

    public long getCounter() {
        return counter;
    }

    public ResponseEventBean setCounter(long counter) {
        this.counter = counter;
        return this;
    }

}
