package fr.ge.common.clicker.ws.v1.bean;

import fr.ge.common.utils.bean.search.SearchResult;

public class SearchResultEvent<T> extends SearchResult<T> {

    private Long totalCounter;

    public SearchResultEvent() {
        super();
    }

    public SearchResultEvent(SearchResult<T> searchResult) {
        super();
        this.setContent(searchResult.getContent());
        this.setMaxResults(searchResult.getMaxResults());
        this.setStartIndex(searchResult.getStartIndex());
        this.setTotalResults(searchResult.getTotalResults());
    }

    public Long getTotalCounter() {
        return totalCounter;
    }

    public SearchResultEvent<T> setTotalCounter(Long totalCounter) {
        this.totalCounter = totalCounter;
        return this;
    }

}
