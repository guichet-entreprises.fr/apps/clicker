package fr.ge.common.clicker.ws.v1.bean;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "reference", namespace = "http://v1.ws.clicker.common.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseReferenceBean {

    /** The id. */
    private Long id;

    /** Functional id. */
    private String ref;

    /** Events. */
    private List<ResponseEventBean> events;

    public String getRef() {
        return ref;
    }

    public ResponseReferenceBean setRef(String ref) {
        this.ref = ref;
        return this;
    }

    public List<ResponseEventBean> getEvents() {
        return events;
    }

    public ResponseReferenceBean setEvents(List<ResponseEventBean> events) {
        this.events = events;
        return this;
    }

    public Long getId() {
        return id;
    }

    public ResponseReferenceBean setId(Long id) {
        this.id = id;
        return this;
    }

}
