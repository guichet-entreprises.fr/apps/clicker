/**
 *
 */
package fr.ge.common.clicker.ws.v1.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ge.common.clicker.ws.v1.bean.SearchResultEvent;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

/**
 * @author amboussa
 *
 */
@Path("/v1/event")
public interface IEventRestService {

    /**
     * create an event
     *
     * @param ref
     *            ref
     * @return
     */
    @POST
    Response create(@QueryParam("ref") String ref, //
            @QueryParam("counter") Long counter, //
            @QueryParam("effectDate") String effectDate, //
            @DefaultValue("") @QueryParam("comment") String comment//
    );

    /**
     * Search for events.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return search result
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    SearchResultEvent search(@QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @QueryParam("filters") List<SearchQueryFilter> filters, //
            @QueryParam("orders") List<SearchQueryOrder> orders //
    );

    @GET
    @Path("/{ref}/count")
    Long count(@PathParam("ref") String reference);
}
