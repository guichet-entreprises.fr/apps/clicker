/**
 *
 */
package fr.ge.common.clicker.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.type.TypeReference;

import fr.ge.common.clicker.service.IReferenceDataService;
import fr.ge.common.clicker.ws.v1.bean.ResponseReferenceBean;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 * @author amboussa
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/data-context.xml", "classpath:spring/ws-server-cxf-context.xml" })
public class ReferenceRestServiceImplTest extends AbstractRestTest {

    @Autowired
    private IReferenceDataService referenceDataService;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * Build reference bean.
     *
     * @return reference bean
     */
    public static ResponseReferenceBean buildReferenceBean() {
        return new ResponseReferenceBean() //
                .setId(1L) //
                .setRef("ref") //
        ;
    }

    /**
     * Test search.
     *
     * @throws Exception
     *             exception
     */
    @Test
    public void testSearch() throws Exception {
        final ResponseReferenceBean dataBean = buildReferenceBean();
        final SearchResult<ResponseReferenceBean> dataSearchResult = new SearchResult<>(1L, 5L);
        dataSearchResult.setTotalResults(1L);
        dataSearchResult.setContent(Arrays.asList(dataBean));

        final SearchQuery searchQuery = new SearchQuery(dataSearchResult.getStartIndex(), dataSearchResult.getMaxResults()).setFilters(new ArrayList<>()).setOrders(new ArrayList<>());
        when(this.referenceDataService.search(eq(searchQuery), eq(ResponseReferenceBean.class))).thenReturn(dataSearchResult);

        final Response response = this.client().accept("application/json") //
                .path("/v1/reference") //
                .query("startIndex", "1") //
                .query("maxResults", "5") //
                .get();

        final SearchResult<ResponseReferenceBean> actualSearchResult = this.readAsBean(response, new TypeReference<SearchResult<ResponseReferenceBean>>() {
        });

        verify(this.referenceDataService).search(any(SearchQuery.class), eq(ResponseReferenceBean.class));

        assertThat(actualSearchResult, allOf( //
                hasProperty("startIndex", equalTo(dataSearchResult.getStartIndex())), //
                hasProperty("maxResults", equalTo(dataSearchResult.getMaxResults())), //
                hasProperty("totalResults", equalTo(dataSearchResult.getTotalResults())) //
        ) //
        );

        assertThat(actualSearchResult.getContent(), //
                contains( //
                        allOf( //
                                Arrays.asList( //
                                        hasProperty("id", equalTo(dataBean.getId())), //
                                        hasProperty("ref", equalTo(dataBean.getRef())) //
                                ) //
                        ) //
                ) //
        );
    }
}
