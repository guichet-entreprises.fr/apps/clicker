/**
 *
 */
package fr.ge.common.clicker.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.clicker.service.IEventDataService;
import fr.ge.common.clicker.ws.v1.bean.ResponseEventBean;
import fr.ge.common.clicker.ws.v1.bean.SearchResultEvent;
import fr.ge.common.clicker.ws.v1.service.IEventRestService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractRestTest;

/**
 * @author amboussa
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/data-context.xml", "classpath:spring/ws-server-cxf-context.xml" })
public class EventRestServiceImplTest extends AbstractRestTest {

    @Autowired
    private IEventRestService eventRestService;

    @Autowired
    private IEventDataService eventDataService;

    @Autowired
    @Qualifier("restServer")
    private JAXRSServerFactoryBean restServerFactory;

    @Override
    protected JAXRSServerFactoryBean getRestServerFactory() {
        return this.restServerFactory;
    }

    /**
     * Testing the creation of an event.
     */
    @Test
    public void testCreateEvent() {
        when(this.eventDataService.createEvent(eq("Reference 1"), eq(1L), any(), eq("comment"))).thenReturn(100L);
        final Response response = this.eventRestService.create("Reference 1", 1L, "20190227", "comment");
        assertThat(response.getEntity(), equalTo(100L));
        assertThat(response.getStatusInfo().getStatusCode(), equalTo(200));
        assertThat(response.getStatusInfo().getReasonPhrase(), equalTo("OK"));
    }

    /**
     * Testing the error response during the creation of an event with a wrong
     * date format.
     */
    @Test
    public void testCreateEventDateFormatError() {
        final Response response = this.eventRestService.create("Reference 1", 1L, "2019/02/27", "comment");
        assertThat(response.getEntity(), equalTo(null));
        assertThat(response.getStatusInfo().getStatusCode(), equalTo(500));
        assertThat(response.getStatusInfo().getReasonPhrase(), equalTo("Internal Server Error"));
    }

    @Test
    public void testSearchWithTotalCounter() {
        final long startIndex = Long.parseLong(SearchQuery.DEFAULT_START_INDEX);
        final long maxResults = Long.parseLong(SearchQuery.DEFAULT_MAX_RESULTS);
        final List<SearchQueryFilter> filters = Arrays.asList(new SearchQueryFilter("updated:ASC"));
        final List<SearchQueryOrder> orders = null;

        final ResponseEventBean clickerBean = new ResponseEventBean().setReference("Reference 1").setCounter(1L);
        final SearchResult<ResponseEventBean> dataSearchResult = new SearchResult<>(startIndex, maxResults);
        dataSearchResult.setTotalResults(3L);
        dataSearchResult.setContent(Arrays.asList(clickerBean, clickerBean, clickerBean));

        // prepare
        when(this.eventDataService.search(any(), eq(ResponseEventBean.class))).thenReturn(dataSearchResult);
        when(this.eventDataService.totalCounter(any())).thenReturn(3L);

        // call
        final SearchResultEvent response = this.eventRestService.search(startIndex, maxResults, filters, orders);

        // verify
        // final SearchQuery searchQuery = new SearchQuery(startIndex,
        // maxResults).setFilters(filters).setOrders(orders);
        assertThat(response.getContent(), equalTo(dataSearchResult.getContent()));
        assertThat(response.getTotalCounter(), equalTo(3L));
    }

    /**
     * Get total count for an event.
     * 
     * @throws Exception
     */
    @Test
    public void testCount() throws Exception {
        when(this.eventDataService.totalCounter(any())).thenReturn(100L);
        assertThat(this.eventRestService.count("Reference"), equalTo(100L));
    }

}
