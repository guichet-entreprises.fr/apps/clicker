/**
 *
 */
package fr.ge.common.clicker.ws.v1.service.impl.managed;

import javax.ws.rs.Path;

import fr.ge.common.clicker.ws.v1.service.impl.ReferenceRestServiceImpl;
import io.swagger.annotations.Api;

/**
 * Clicker reference managed services
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api("Clicker Managed reference Rest Services")
@Path("/v1/reference")
public class ReferenceManagedRestServiceImpl extends ReferenceRestServiceImpl {

}
