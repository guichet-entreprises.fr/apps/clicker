/**
 *
 */
package fr.ge.common.clicker.ws.v1.service.impl.managed;

import javax.ws.rs.Path;

import fr.ge.common.clicker.ws.v1.service.impl.EventRestServiceImpl;
import io.swagger.annotations.Api;

/**
 * Clicker event managed services
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Api("Clicker Managed event Rest Services")
@Path("/v1/event")
public class EventManagedRestServiceImpl extends EventRestServiceImpl {

}
