/**
 *
 */
package fr.ge.common.clicker.ws.v1.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.clicker.service.IEventDataService;
import fr.ge.common.clicker.ws.v1.bean.ResponseEventBean;
import fr.ge.common.clicker.ws.v1.bean.SearchResultEvent;
import fr.ge.common.clicker.ws.v1.service.IEventRestService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author amboussa
 *
 */
@Api("Clicker Event Rest Services")
@Path("/v1/event")
public class EventRestServiceImpl implements IEventRestService {

    @Autowired
    private IEventDataService eventDataService;

    /** The Constant DEFAULT_COUNTER. */
    public static final String DEFAULT_COUNTER = "0";

    /** Application date format. */
    private static final String APPLICATION_DATE_FORMAT = "yyyyMMdd";

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Create a new event", notes = "Add a new event for the reference defined.<br/>" + "If the reference does not exist, it will be created.<br/>")
    public Response create(@ApiParam("The reference of the event, can be a new one.") final String reference,
            @ApiParam("The value to change the sum of all event's counter from the same reference") @DefaultValue(DEFAULT_COUNTER) final Long counter,
            @ApiParam("The date event's counter value applies to the overall counter") final String effectDate, @ApiParam("Optional comment about this event") final String comment) {
        final Calendar applicationCalendar = Calendar.getInstance();
        try {
            final Date applicationDate = new SimpleDateFormat(APPLICATION_DATE_FORMAT).parse(effectDate);
            applicationCalendar.setTime(applicationDate);
        } catch (final ParseException e) {
            return Response.serverError().build();
        }
        return Response.ok(this.eventDataService.createEvent(reference, counter, applicationCalendar, comment)).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for queue messages", notes = "Multiple filters can be specified using pattern<br/>" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :<br/>" //
            + "- \":\" = equals<br/>" //
            + " (for multiple values, insert a \\\",\\\" between each value. --> fieldName:value1, value2)<br/>" //
            + "- \"&gt;\" = greater than<br/>" //
            + "- \"&gt;=\" = greater than or equals<br/>" //
            + "- \"&lt;\" = less than<br/>" //
            + "- \"&lt;=\" = less than or equals<br/>")
    public SearchResultEvent search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders) {

        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders);
        SearchResult<ResponseEventBean> resultSearch = this.eventDataService.search(searchQuery, ResponseEventBean.class);

        SearchResultEvent result = new SearchResultEvent(resultSearch);
        result.setTotalCounter(this.eventDataService.totalCounter(searchQuery));

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long count(final String reference) {
        return this.eventDataService.totalCounter(new SearchQuery().addFilter("reference:" + reference));
    }
}
