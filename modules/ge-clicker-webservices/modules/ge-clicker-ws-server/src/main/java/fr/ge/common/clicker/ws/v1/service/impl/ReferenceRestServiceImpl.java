package fr.ge.common.clicker.ws.v1.service.impl;

import java.util.List;

import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.clicker.service.IReferenceDataService;
import fr.ge.common.clicker.ws.v1.bean.ResponseReferenceBean;
import fr.ge.common.clicker.ws.v1.service.IReferenceRestService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import io.swagger.annotations.Api;

@Api("Clicker References Rest Services")
@Path("/v1/reference")
public class ReferenceRestServiceImpl implements IReferenceRestService {

    @Autowired
    private IReferenceDataService referenceDataService;

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResult<ResponseReferenceBean> search(final long startIndex, final long maxResults, final List<SearchQueryFilter> filters, final List<SearchQueryOrder> orders) {
        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders);
        return this.referenceDataService.search(searchQuery, ResponseReferenceBean.class);
    }

}
