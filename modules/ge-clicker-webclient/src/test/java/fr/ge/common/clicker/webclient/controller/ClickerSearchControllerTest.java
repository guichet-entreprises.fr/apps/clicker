/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.clicker.webclient.controller;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.clicker.ws.v1.bean.ResponseEventBean;
import fr.ge.common.clicker.ws.v1.bean.SearchResultEvent;
import fr.ge.common.clicker.ws.v1.service.IEventRestService;
import fr.ge.common.clicker.ws.v1.service.IReferenceRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * @author amboussa
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class ClickerSearchControllerTest {

    /** mvc. */
    private MockMvc mvc;

    /** web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    @Autowired
    private IEventRestService clickService;

    @Autowired
    private IReferenceRestService referenceService;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.clickService);
        reset(this.referenceService);
    }

    @Test
    public void testSearchMain() throws Exception {
        this.mvc.perform(MockMvcRequestBuilders.get("/event")) //
                .andExpect(MockMvcResultMatchers.status().isOk()) //
                .andExpect(MockMvcResultMatchers.view().name("event/search/main"));
    }

    @Test
    public void testSearchEmptyData() throws Exception {
        final SearchResultEvent<ResponseEventBean> searchResult = new SearchResultEvent(new SearchResult<ResponseEventBean>(0, 10));
        when(this.clickService.search(any(long.class), any(long.class), any(), any())).thenReturn(searchResult);

        this.mvc.perform( //
                get("/event/search/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
                        .param("filters", "ref!") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.clickService).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture());

        assertThat(filtersCaptor.getValue(), hasSize(1));
        assertThat(ordersCaptor.getValue(), empty());
    }

    @Test
    public void testSearchData() throws Exception {
        final SearchResultEvent<ResponseEventBean> searchResult = new SearchResultEvent(new SearchResult<ResponseEventBean>(0, 10));
        searchResult.setContent(Collections.singletonList(new ResponseEventBean()));

        when(this.clickService.search(any(long.class), any(long.class), any(), any())).thenReturn(searchResult);

        this.mvc.perform( //
                get("/event/search/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.clickService).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture());

        assertThat(filtersCaptor.getValue(), hasSize(0));
        assertThat(ordersCaptor.getValue(), empty());
    }

}
