/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2014-2016)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
requirejs.config({
    baseUrl: baseUrl.replace(/\/$/, '') + '/js',
    paths: {
        app: 'app',
        i18n: 'i18n',
        rest: '../rest',
    },
    map: {
        '*': '*'
    }
});

requirejs([ 'jquery', 'bootstrap', 'lib/polyfill', 'lib/datatables' ], function($) {

    window.ajaxTypeSearch = function(data, callback, settings) {
		var reference = $("#searchName").val();
    	var dateMinFormatted = null;
    	var dateMaxFormatted = null;
    	
    	if($("#date-min-event").val().length > 0) {
    		var dateMin = new Date($("#date-min-event").datepicker('getDate'));
        	var monthMin = dateMin.getMonth() + 1;
        	var dayMin = dateMin.getDate();
        	if(monthMin < 10) {
        		monthMin = "0"+ monthMin;
        	}
        	if(dayMin < 10) {
        		dayMin = "0"+dayMin;
        	}
        	var dateMinFormatted = dateMin.getFullYear()+""+monthMin+""+dayMin;
    	}   	
        
    	if($("#date-max-event").val().length > 0) {
    		var dateMax = new Date($("#date-max-event").datepicker('getDate'));
        	var monthMax = dateMax.getMonth() + 1;
        	var dayMax = dateMax.getDate();
        	if(monthMax < 10) {
        		monthMax = "0"+ monthMax;
        	}
        	if(dayMax < 10) {
        		dayMax = "0"+dayMax;
        	}
        	var dateMaxFormatted = dateMax.getFullYear()+""+monthMax+""+dayMax;
    	}
    	
    	var filters = [];
    	if((reference != null) && (reference.length > 0)) {
    		filters.push("reference:"+reference);
    	}
    	if(dateMinFormatted != null) {
    		filters.push("effectDateMin>="+dateMinFormatted);
    	}
    	if(dateMaxFormatted != null) {
    		filters.push("effectDateMax<="+dateMaxFormatted);
    	}

        $.ajax(baseUrl + 'event/search/data?filters='+filters+'', {
            method : "GET",
            data : data,
            cache : false
        }).done(function(res) {
            var objectVersions = [];
            $.each(res.data, function(index, value) {
                objectVersions.push(value);
            });
            $("#total-counter").text(res.totalCounter);
            res.data = objectVersions;
            callback && callback(res);
        });
    };

    $('table[data-toggle="datatable"]').DataTable();

});

