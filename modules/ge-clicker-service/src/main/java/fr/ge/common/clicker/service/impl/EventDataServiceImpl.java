/**
 *
 */
package fr.ge.common.clicker.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.clicker.service.IEventDataService;
import fr.ge.common.clicker.service.bean.EventBean;
import fr.ge.common.clicker.service.bean.ReferenceBean;
import fr.ge.common.clicker.service.mapper.IEventMapper;
import fr.ge.common.clicker.service.mapper.IReferenceMapper;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * The Class EventDataServiceImpl.
 *
 * @author amboussa
 */
@Service
public class EventDataServiceImpl implements IEventDataService {

    /**
     *
     */
    private static final int FUNC_ID_LENGTH = 255;
    private static final String REFERENCE = "reference";

    /** The event mapper. */
    @Autowired
    private IEventMapper eventMapper;

    /** The reference mapper. */
    @Autowired
    private IReferenceMapper referenceMapper;

    /** The dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * {@inheritDoc}
     */
    @Override
    public Long createEvent(final String reference, final long counter, final Calendar effectDate, final String comment) {
        ReferenceBean referenceEvent = this.referenceMapper.findByRef(reference);
        if (referenceEvent == null) {
            referenceEvent = new ReferenceBean();
            referenceEvent.setRef(StringUtils.substring(reference, 0, FUNC_ID_LENGTH - 1));
            this.referenceMapper.create(referenceEvent);
        }

        final Calendar now = Calendar.getInstance();
        final EventBean event = new EventBean().setCreated(now).setUpdated(now).setCounter(counter).setEffectDate(effectDate).setComment(comment);

        this.getOrCreate(event, referenceEvent.getId());

        return event.getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getOrCreate(final EventBean entity, final Long refId) {
        return this.eventMapper.create(entity, refId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EventBean findById(final Long id) {
        return this.eventMapper.findById(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<EventBean> findByReference(final String reference) {
        return this.eventMapper.findByReference(reference);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(final SearchQuery searchQuery, final Class<R> expectedClass) {
        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<SearchQueryFilter> filters = Optional.ofNullable(searchQuery.getFilters()).filter(f -> null != f).orElse(new ArrayList<SearchQueryFilter>());
        final List<SearchQueryOrder> orders = Optional.ofNullable(searchQuery.getOrders()).filter(o -> null != o).orElse(new ArrayList<SearchQueryOrder>());
        final List<EventBean> entities = this.eventMapper.findAll( //
                filters, //
                orders, //
                rowBounds);

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            searchResult.setContent(entities.stream().map(elm -> this.dozer.map(elm, expectedClass)).collect(Collectors.toList()));
        }

        searchResult.setTotalResults(this.eventMapper.count(filters));

        return searchResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReferenceBean findReferenceByRef(final String ref) {
        return this.referenceMapper.findByRef(ref);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReferenceBean findReferenceById(final Long id) {
        return this.referenceMapper.findById(id);
    }

    @Override
    public Long totalCounter(final SearchQuery searchQuery) {
        return this.eventMapper.totalCounter(searchQuery.getFilters());
    }
}
