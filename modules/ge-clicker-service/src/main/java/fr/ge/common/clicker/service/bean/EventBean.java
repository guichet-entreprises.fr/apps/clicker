/**
 *
 */
package fr.ge.common.clicker.service.bean;

import java.util.Calendar;

/**
 * The Class EventBean.
 *
 * @author amboussa
 */
public class EventBean extends AbstractDatedBean<EventBean> {

    /** the ref id of event. */
    private String reference;

    /** the counter of event. */
    private long counter;

    /** the date of application of event. */
    private Calendar effectDate;

    /** a comment about the event. */
    private String comment;

    /**
     * Instantiates a new event bean.
     */
    public EventBean() {
        super();
    }

    /**
     * Gets the reference.
     *
     * @return the reference
     */
    public String getReference() {
        return this.reference;
    }

    /**
     * Sets the reference.
     *
     * @param reference
     *            the new reference
     * @return the event bean
     */
    public EventBean setReference(final String reference) {
        this.reference = reference;
        return this;
    }

    /**
     * Gets the counter.
     *
     * @return the counter
     */
    public long getCounter() {
        return this.counter;
    }

    /**
     * Sets the counter.
     *
     * @param counter
     */
    public EventBean setCounter(final long counter) {
        this.counter = counter;
        return this;
    }

    /**
     * Gets the date of application.
     *
     * @return the date of application
     */
    public Calendar getEffectDate() {
        return this.effectDate;
    }

    /**
     * Sets the date of application.
     *
     * @param application
     */
    public EventBean setEffectDate(final Calendar application) {
        this.effectDate = application;
        return this;
    }

    /**
     * Gets the comment.
     * 
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the comment.
     * 
     * @param comment
     */
    public EventBean setComment(String comment) {
        this.comment = comment;
        return this;
    }

}