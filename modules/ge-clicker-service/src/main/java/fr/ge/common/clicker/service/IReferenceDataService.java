/**
 *
 */
package fr.ge.common.clicker.service;

import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * The Interface IReferenceDataService.
 *
 * @author amboussa
 */
public interface IReferenceDataService {

    /**
     * Search for events.
     *
     * @param <R>
     *            the generic type
     * @param query
     *            search parameters
     * @param expectedClass
     *            the expected class
     * @return search result
     */
    <R> SearchResult<R> search(SearchQuery query, Class<R> expectedClass);

}
