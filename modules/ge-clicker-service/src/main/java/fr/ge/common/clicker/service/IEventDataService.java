/**
 *
 */
package fr.ge.common.clicker.service;

import java.util.Calendar;
import java.util.List;

import fr.ge.common.clicker.service.bean.EventBean;
import fr.ge.common.clicker.service.bean.ReferenceBean;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * The Interface IEventDataService.
 *
 * @author amboussa
 */
public interface IEventDataService {

    /**
     * create an event.
     *
     * @param ref
     *            ref
     * @return created row count (expecting 1)
     */
    Long createEvent(String ref, long counter, Calendar effectDate, String comment);

    /**
     * Creates the EventBean.
     *
     * @param entity
     *            the EventBean
     * @param ref
     *            the ref id
     * @return created row count (expecting 1)
     */
    long getOrCreate(final EventBean entity, final Long ref);

    /**
     * Find by id.
     *
     * @param id
     *            the id
     * @return the event bean
     */
    EventBean findById(Long id);

    /**
     * Find by reference.
     *
     * @param ref
     *            the reference
     * @return the list
     */
    List<EventBean> findByReference(String reference);

    /**
     * Search for events.
     *
     * @param <R>
     *            the generic type
     * @param query
     *            search parameters
     * @param expectedClass
     *            the expected class
     * @return search result
     */
    <R> SearchResult<R> search(SearchQuery query, Class<R> expectedClass);

    /**
     * Find reference by the ref id.
     *
     * @param ref
     *            the ref id
     * @return the reference bean
     */
    ReferenceBean findReferenceByRef(final String ref);

    /**
     * Find reference by the id.
     *
     * @param id
     *            the id
     * @return the reference bean
     */
    ReferenceBean findReferenceById(final Long id);

    /**
     * 
     * @param query
     * @return
     */
    Long totalCounter(SearchQuery searchQuery);
}
