/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.clicker.service.handler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Calendar;
import java.util.Optional;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CalendarTypeHandler.
 *
 * @author Christian Cougourdan
 */
public class CalendarTypeHandler implements TypeHandler<Calendar> {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(CalendarTypeHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void setParameter(final PreparedStatement ps, final int i, final Calendar parameter, final JdbcType jdbcType) throws SQLException {
        LOGGER.debug("Set parameter {} : {}", i, parameter);
        if (null == parameter) {
            ps.setNull(i, Types.TIMESTAMP);
        } else {
            ps.setTimestamp(i, new Timestamp(parameter.getTimeInMillis()));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar getResult(final ResultSet rs, final String columnName) throws SQLException {
        final Timestamp ts = rs.getTimestamp(columnName);
        return Optional.ofNullable(ts).map(millis -> this.buildCalendar(millis.getTime())).orElse(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar getResult(final ResultSet rs, final int columnIndex) throws SQLException {
        final Timestamp ts = rs.getTimestamp(columnIndex);
        return Optional.ofNullable(ts).map(millis -> this.buildCalendar(millis.getTime())).orElse(null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Calendar getResult(final CallableStatement cs, final int columnIndex) throws SQLException {
        final Timestamp ts = cs.getTimestamp(columnIndex);
        return Optional.ofNullable(ts).map(millis -> this.buildCalendar(millis.getTime())).orElse(null);
    }

    /**
     * Builds the calendar.
     *
     * @param millis
     *            calendar in milliseconds
     * @return the calendar
     */
    private Calendar buildCalendar(final long millis) {
        final Calendar dateTime = Calendar.getInstance();
        dateTime.setTimeInMillis(millis);
        return dateTime;
    }

}
