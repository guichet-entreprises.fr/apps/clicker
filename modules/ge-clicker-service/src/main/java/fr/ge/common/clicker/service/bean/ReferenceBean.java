/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.clicker.service.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * The Class ReferenceBean.
 *
 * @author amboussa
 */
public class ReferenceBean {

    /** The id. */
    private Long id;

    /** Functional id. */
    private String ref;

    /** Events. */
    private List<EventBean> events;

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

    /**
     * Copies this object.
     *
     * @return a copy
     */
    public ReferenceBean copy() {
        final ReferenceBean reference = new ReferenceBean();
        reference.setId(this.getId());
        reference.setRef(this.ref);
        reference.setEvents(new ArrayList<>(this.events));
        return reference;
    }

    /**
     * Getter on attribute {@link #ref}.
     *
     * @return String ref
     */
    public String getRef() {
        return this.ref;
    }

    /**
     * Setter on attribute {@link #ref}.
     *
     * @param ref
     *            the new value of attribute ref
     * @return the reference bean
     */
    public ReferenceBean setRef(final String ref) {
        this.ref = ref;
        return this;
    }

    /**
     * Getter on attribute {@link #clickers}.
     *
     * @return List<ClickerBean> clickers
     */
    public List<EventBean> getEvents() {
        return this.events;
    }

    /**
     * Setter on attribute {@link #clickers}.
     *
     * @param clickers
     *            the new value of attribute clickers
     * @return the reference bean
     */
    public ReferenceBean setEvents(final List<EventBean> clickers) {
        this.events = Optional.ofNullable(clickers).map(lst -> new ArrayList<>(lst)).orElse(null);
        return this;
    }

    /**
     * 
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     * 
     * @param id
     */
    public ReferenceBean setId(Long id) {
        this.id = id;
        return this;
    }

}
