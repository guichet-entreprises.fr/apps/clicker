/**
 *
 */
package fr.ge.common.clicker.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ibatis.session.RowBounds;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.clicker.service.IReferenceDataService;
import fr.ge.common.clicker.service.bean.ReferenceBean;
import fr.ge.common.clicker.service.mapper.IReferenceMapper;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Reference service.
 * 
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service
public class ReferenceDataServiceImpl implements IReferenceDataService {

    /** The reference mapper. */
    @Autowired
    private IReferenceMapper referenceMapper;

    /** The dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(final SearchQuery searchQuery, final Class<R> expectedClass) {
        final Map<String, Object> filters = new HashMap<>();
        if (null != searchQuery.getFilters()) {
            searchQuery.getFilters().forEach(filter -> filters.put(filter.getColumn(), filter));
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<ReferenceBean> entities = this.referenceMapper.findAll(filters, searchQuery.getOrders(), rowBounds);

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            searchResult.setContent(entities.stream().map(elm -> this.dozer.map(elm, expectedClass)).collect(Collectors.toList()));
        }

        searchResult.setTotalResults(this.referenceMapper.count(filters));

        return searchResult;
    }
}
