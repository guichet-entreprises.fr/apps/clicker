/**
 *
 */
package fr.ge.common.clicker.service.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import fr.ge.common.clicker.service.bean.EventBean;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

/**
 * The Interface IEventMapper.
 *
 * @author amboussa
 */
public interface IEventMapper {

    /**
     * Creates the EventBean.
     *
     * @param entity
     *            the EventBean
     * @param refId
     *            the ref id
     * @return created row count (expecting 1)
     */
    long create(@Param("event") EventBean entity, @Param("reference") Long refId);

    /**
     * Find by id.
     *
     * @param id
     *            the id
     * @return the event bean
     */
    EventBean findById(@Param("id") Long id);

    /**
     * Find by rreference.
     *
     * @param reference
     *            the reference
     * @return the list
     */
    List<EventBean> findByReference(@Param("reference") String reference);

    /**
     * Search for events.
     *
     * @param filters
     *            filters
     * @param orders
     *            orders
     * @param rowBounds
     *            pagination
     * @return queue messages list
     */
    List<EventBean> findAll(@Param("filters") List<SearchQueryFilter> filters, @Param("orders") List<SearchQueryOrder> orders, RowBounds rowBounds);

    /**
     * Count events.
     *
     * @param filters
     *            filters
     * @return total elements corresponding
     */
    long count(@Param("filters") List<SearchQueryFilter> filters);

    /**
     * 
     * @param filters
     * @return
     */
    Long totalCounter(@Param("filters") List<SearchQueryFilter> filters);

}
