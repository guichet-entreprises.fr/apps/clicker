ALTER TABLE event
ADD comment VARCHAR(255);

ALTER TABLE event
	RENAME COLUMN application TO effect_date ;