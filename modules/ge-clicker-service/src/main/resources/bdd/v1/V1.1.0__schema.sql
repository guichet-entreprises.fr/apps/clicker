
CREATE TABLE reference (
    id              BIGINT          PRIMARY KEY,
    func_id         VARCHAR(255)    UNIQUE NOT NULL
);

CREATE SEQUENCE sq_reference
    START 1
    MINVALUE 1
    CACHE 1;


CREATE TABLE event (
    id           BIGINT      NOT NULL,
    ref_id       BIGINT      NOT NULL,
    counter      BIGINT      NOT NULL,
    created      TIMESTAMP   NOT NULL,
    updated      TIMESTAMP   NOT NULL,
    application  TIMESTAMP   NOT NULL
);

CREATE SEQUENCE sq_event
    START 1
    MINVALUE 1
    CACHE 1;

ALTER TABLE event
    ADD CONSTRAINT fk_event_reference
    FOREIGN KEY (ref_id)
    REFERENCES reference(id);

