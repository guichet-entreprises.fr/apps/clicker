DROP INDEX IF EXISTS "idx_event_ref_effect";
CREATE INDEX "idx_event_ref_effect" ON "event" (ref_id, effect_date);

DROP INDEX IF EXISTS "idx_event_effect";
CREATE INDEX "idx_event_effect" ON "event" (effect_date);

DROP INDEX IF EXISTS "idx_reference_func";
CREATE INDEX "idx_reference_func" ON "reference" (func_id);
