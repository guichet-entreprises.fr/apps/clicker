/**
 *
 */
package fr.ge.common.clicker.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.clicker.service.IEventDataService;
import fr.ge.common.clicker.service.bean.EventBean;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.test.AbstractPostgreSqlDbTest;

/**
 * @author amboussa
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class EventDataServiceImplTest extends AbstractPostgreSqlDbTest {

    @Autowired
    private IEventDataService eventDataService;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    @Test
    public void testCreate() {
        final Calendar now = Calendar.getInstance();
        final EventBean event = new EventBean().setCreated(now).setUpdated(now).setCounter(1L).setEffectDate(Calendar.getInstance());

        this.eventDataService.getOrCreate(event, 200L);
    }

    @Test
    public void testFindById() {
        assertThat(this.eventDataService.findById(100L), allOf(Arrays.asList( //
                hasProperty("id", equalTo(100L)), //
                hasProperty("reference", equalTo("Reference 1")) //
        )));
    }

    @Test
    public void testFindByReference() {
        assertThat(this.eventDataService.findByReference("Reference 1"), contains( //
                Arrays.asList( //
                        allOf( //
                                hasProperty("id", equalTo(100L)), //
                                hasProperty("reference", equalTo("Reference 1")) //
                        ) //
                )) //
        );
    }

    @Test
    public void testSearch() {
        final SearchQuery searchQuery = new SearchQuery(0L, 5L);
        searchQuery.setOrders(new ArrayList<>());
        final SearchResult<EventBean> actual = this.eventDataService.search(searchQuery, EventBean.class);

        assertThat(actual, //
                allOf( //
                        hasProperty("startIndex", equalTo(0L)), //
                        hasProperty("maxResults", equalTo(5L)), //
                        hasProperty("totalResults", equalTo(2L))));
    }

    @Test
    public void testFindReferenceById() {
        assertThat(this.eventDataService.findReferenceById(200L), allOf(Arrays.asList( //
                hasProperty("id", equalTo(200L)), //
                hasProperty("ref", equalTo("Reference 1"))//
        )));
    }

    @Test
    public void testFindReferenceByFuncId() {
        assertThat(this.eventDataService.findReferenceByRef("Reference 1"), allOf(Arrays.asList( //
                hasProperty("id", equalTo(200L)), //
                hasProperty("ref", equalTo("Reference 1"))//
        )));
    }

    /**
     * Dans ce TU on test l'ajout d'un évènement avec un nom de référence
     * existante en base :
     * 
     * - 1) On vérifie que le nom de la référence utilisé existe en base
     * 
     * - 2) On ajoute un évènement en base et on vérifie qu'il a bien été créé
     * en base
     */
    @Test
    public void testAjoutEventAvecRefExistante() {
        Calendar now = Calendar.getInstance();

        assertThat(this.eventDataService.findReferenceByRef("Reference 1"), allOf(Arrays.asList( //
                hasProperty("id", equalTo(200L)), //
                hasProperty("ref", equalTo("Reference 1"))//
        )));

        Long idNewEvent = this.eventDataService.createEvent("Reference 1", 1L, now, "");

        assertThat(this.eventDataService.findById(idNewEvent), allOf(Arrays.asList( //
                hasProperty("id", equalTo(idNewEvent)), //
                hasProperty("counter", equalTo(1L)), //
                hasProperty("reference", equalTo("Reference 1")) //
        )));
    }

    /**
     * Dans ce TU on test l'ajout d'un évènement avec un nom de référence qui
     * n'existe pas en base :
     * 
     * - 1) On vérifie que le nom de la référence utilisé n'existe PAS en base
     * 
     * - 2) On ajoute un évènement en base et on vérifie qu'il a bien été créé
     * en base
     * 
     * - 3) On vérifie qu'une nouvelle référence a été créée en base
     */
    @Test
    public void testAjoutEventAvecNouvelleRef() {
        Calendar now = Calendar.getInstance();

        assertThat(this.eventDataService.findReferenceByRef("Reference New"), equalTo(null));

        Long idNewEvent = this.eventDataService.createEvent("Reference New", 2L, now, "Adding a comment");

        assertThat(this.eventDataService.findById(idNewEvent), allOf(Arrays.asList( //
                hasProperty("id", equalTo(idNewEvent)), //
                hasProperty("counter", equalTo(2L)), //
                hasProperty("reference", equalTo("Reference New")), //
                hasProperty("comment", equalTo("Adding a comment")) //
        )));

        assertThat(this.eventDataService.findReferenceByRef("Reference New"), allOf(Arrays.asList( //
                hasProperty("ref", equalTo("Reference New"))//
        )));
    }

    /**
     * 
     */
    @Test
    public void testTotalCounter() {
        final SearchQuery searchQuery = new SearchQuery();
        SearchQueryFilter filter = new SearchQueryFilter("reference", ":", "Reference 1");
        searchQuery.addFilter(filter);
        searchQuery.setOrders(new ArrayList<>());
        final Long result = this.eventDataService.totalCounter(searchQuery);

        assertThat(result, equalTo(1L));
    }
}
