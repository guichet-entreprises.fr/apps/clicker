# Description de Clicker

Clicker est un outil qui permet de récolter et restituer des évènements enregistrés.

Un évènement est défini par :
*	Un label qui définit le type d’évènement --> La référence de l'évènement
*	Une valeur numérique qui indique le nombre à additionner ou soustraire au compteur du type d’évènement
*	Une date d’effet de l’évènement
*	Un champ de commentaire libre

---

# Contenu de l'application
Application qui contient : 
* Des WS pour créer et restituer les Evènements
* Un WS pour restituer la liste des Références (La Réfèrence est l'objet qui représente un type d'évènement)
* Une IHM sous forme de tableau pour afficher et filtrer les Evènements

#### Attributs d'un Evènement 

| Nom | Description |
| :--- | :--- |
| created | Date de création de l'évènement | 
| updated | Date de création de l'évènement | 
| reference | Référence de l'évènement (Label du type d'évènement) | 
| comment | Commentaire de l'évènement | 
| counter | Variation de la valeur du compteur de l'évènement (par exemple "1", "2", "-3" etc...) | 
| effectDate | Date d'effet de l'évènement | 

#### Attributs d'une Référence 

| Nom | Description |
| :--- | :--- |
| ref | Label de la référence | 
| events | Liste des évènements de la référence | 

---

## Services Rest des Evènements
### Recherche des Evènements (GET /v1/event)

Le service de recherche est une recherche avec pagination côté serveur avec la possibilité de trier et filtrer les résultats.

| Paramètre | Type | Description |
| :--- | :--- | :--- |
| startIndex | `Integer` | Index de la page  |
| maxResults | `Integer` | Nombre d'éléments maximum par page |
| filters | `array[string]` | 1 ou plusieurs filtres sous la forme `<fieldName><operator><value>` |
| orders | `array[string]` | Tri sous la forme `<fieldName>:<asc|desc>` |


Exemple : 
~~~
http://localhost:11080/ge-clicker-ws-server/api/v1/event?startIndex=0&maxResults=20&filters=reference%3ARef%201%2Ctest&orders=reference%3Aasc
~~~

#### Paramètres des filtres

fieldName : 
* created --> valeur du filtre au format **yyyyMMdd**
* updated --> valeur du filtre au format **yyyyMMdd**
* reference
* comment
* counter 
* effect_date --> valeur du filtre au format **yyyyMMdd**

operator : 
* “:” = égal à
(pour des valeurs multiples, insérer une “,” entre chaque valeur. --> fieldName:value1, value2)
* “>” = Supérieur à
* “>=” = Supérieur ou égal à
* “<” = Inférieur à
* “<=” = Inférieur ou égal à

### Création d'un Evènement (POST /v1/event)

Ajout d'un évènement pour la Référence définie. Si la Référence n'existe pas en base elle est créée.

| Paramètre | Type | Description |
| :--- | :--- | :--- |
| ref | `String` | Référence (type) de l'évènement  |
| counter | `Integer` | Valeur de variation du compteur pour l'évènement |
| effectDate | `String` | Date d'effet de l'évènement, au format **yyyyMMdd** |
| comment | `String` | Commentaire optionnel de l'évènement |

Exemple : 
~~~
http://localhost:11080/ge-clicker-ws-server/api/v1/event?ref=Test%202&counter=1&effectDate=20190528&comment=test%20test%20
~~~

---

## Services Rest des Références
### Recherche des Evènements (GET /v1/reference)
Le service de recherche des références est similaire au service de recherche des Evènements.

| Paramètre | Type | Description |
| :--- | :--- | :--- |
| startIndex | `Integer` | Index de la page  |
| maxResults | `Integer` | Nombre d'éléments maximum par page |
| filters | `array[string]` | 1 ou plusieurs filtres sous la forme `<fieldName><operator><value>` |
| orders | `array[string]` | Tri sous la forme `<fieldName>:<asc|desc>` |


Exemple : 
~~~
http://localhost:11080/ge-clicker-ws-server/api/v1/reference?startIndex=0&maxResults=20
~~~

#### Paramètres des filtres

fieldName : 
* ref

operator : 
* “:” = égal à
(pour des valeurs multiples, insérer une “,” entre chaque valeur. --> fieldName:value1, value2)
* “>” = Supérieur à
* “>=” = Supérieur ou égal à
* “<” = Inférieur à
* “<=” = Inférieur ou égal à

--- 

## Interface IHM de Clicker

Une IHM est disponible pour l'application Clicker.

[Clicker sur l'environnement de DEV](https://clicker.dev.guichet-partenaires.fr/event) 

L'IHM est composée :
* D'un tableau avec une pagination côté serveur qui liste les évènements 
* Un espace de recherche pour filtrer le contenu du tableau

### Espace de filtrage
Dans cette partie de l'IHM il est possible de filtrer par :
* 0 ou 1 type de Référence --> Soit tous les types d'évènement sont montrés soit juste un seul
* La date d'effet de l'évènement comprise dans un intervalle de date. Il est possible de ne renseigner que la date minimum ou maximum pour récupérer tous les évènements avant ou après une certaine date.

### Tableau de restitution des évènements
Le tableau liste l'ensemble des évènements (filtrés ou non). Pour chaque évènement les informations suivantes sont affichées :
* La référence de l'évènement
* La date d'application
* Le compteur associé à l'évènement 
* Le commentaire

En dessous du tableau sous la colonne "Compteur" est affiché le total de toutes les valeurs de la colonne.

---

# Utilisation de Clicker dans une FA/transition

Il est possible d'ajouter la création d'évènements clicker directement dans le code javascript (que ce soit dans les FA ou dans les transitions).

````
var response = nash.service.request('${ws.clicker.url}/v1/event') //
.param("ref", reference) //
.param("counter", counter) //
.param("effectDate", applicationDateStr) // applicationDateStr est une date au format yyyyMMdd
.param("comment", comment) //
.post(null);
````

---
# FA de Régularisation de click

Dans le git ge-formalities est disponible une **FA de Régulation d'évènement clicker**.

C'est une FA pour le support, qui permet d'ajouter manuellement un évènement pour réguler le compteur qui a été modifié par un autre évènement qui n'est finalement plus/pas voulu.

---
# Algorithme de récupération des Noms d'évènements clicker à la transmission d'un dossier

Dans le git ge-directory, un algorithme **"clicker dossier transmis"** a été ajouté.
Le but de cet algorithme est de retourner la liste de noms de références à utiliser pour créer des évènements clicker. L'algorithme ne fait pas 
Ces noms de référence sont composés de : 
* La base "DOSSIER_TRANSMIS"
* Le réseau (GREFFE, CMA, CCI ou CA)
* Le role (CFE, TDR)
* Le code edi

Par exemple pour la transmission d'un dossier à une seule autorité compétente on peut récupérer la liste suivante :
* DOSSIER_TRANSMIS
* DOSSIER_TRANSMIS_CA
* DOSSIER_TRANSMIS_CA_CFE
* DOSSIER_TRANSMIS_CA_CFE_X7501

Dans le cas où on doit transmettre le dossier à plusieurs autorités compétentes, on récupère autant de fois la liste précédente adaptée pour chaque autorité compétente.
Par exemple si le dossier est transmis à une CA et à une GREFFE on peut avoir:
* DOSSIER_TRANSMIS
* DOSSIER_TRANSMIS_CA
* DOSSIER_TRANSMIS_CA_CFE
* DOSSIER_TRANSMIS_CA_CFE_X7501
* DOSSIER_TRANSMIS
* DOSSIER_TRANSMIS_GREFFE
* DOSSIER_TRANSMIS_GREFFE_CFE-TDR
* DOSSIER_TRANSMIS_GREFFE_CFE-TDR_X7501
