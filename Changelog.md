# Changelog
Ce fichier contient les modifications techniques du module.

- Projet : [Clicker](https://gitlab.com/guichet-entreprises.fr/apps/clicker)

## [2.16.1.0] - 2020-09-11

###  Ajout

- Modifier le logo de la bannière

__Properties__ :

### Ajout

| fichier   |      nom      | nouvelle valeur |
|-----------|:-------------:|----------------:|
| application.properties | ui.theme.default | default |
| application.properties | ui.theme.defaultSubTheme | guichet-entreprises |

## [2.14.3.0] - 2020-04-06

### User stories and enablers

MINE-1284 : Correction de l'API de recherche de clics

###  Ajout

### Modification

### Suppression

## Liens

[2.14.3.0](https://tools.projet-ge.fr/gitlab/minecraft/ge-clicker/tags/ge-clicker-2.14.3.0)

## [2.13.1.0] - 2019-12-05

###  Ajout

Ouvrir les API Clicker

### Modification

### Suppression

## Liens

[2.13.1.0](https://tools.projet-ge.fr/gitlab/minecraft/ge-clicker/tags/ge-clicker-2.13.1.0)
